<?php

	session_start();

	require('src/log.php');

	
	if(!empty($_POST['email']) && !empty($_POST['password'])){

		require('src/connect.php');

		// VARIABLES
		$email 			= htmlspecialchars($_POST['email']);
		$password		= htmlspecialchars($_POST['password']);

		// ADRESSE EMAIL SYNTAXE
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {

			header('location: index.php?error=1&message=Votre adresse email est invalide.');
			exit();

		}

		// CHIFFRAGE DU MOT DE PASSE
		$password = "aq1".sha1($password."123")."25";

		// EMAIL DEJA UTILISE
		$req = $db->prepare("SELECT count(*) as numberEmail FROM user WHERE email = ?");
		$req->execute(array($email));

		while($email_verification = $req->fetch()){
			if($email_verification['numberEmail'] != 1){
				header('location: index.php?error=1&message=Impossible de vous authentifier correctement.');
				exit();
			}
		}

		// CONNEXION
		$req = $db->prepare("SELECT * FROM user WHERE email = ?");
		$req->execute(array($email));

		while($user = $req->fetch()){

			if($password == $user['password'] && $user['blocked'] == 0){  // BLOQUAGE USER -> && $user['blocked'] == 0

				$_SESSION['connect'] = 1;
				$_SESSION['email']   = $user['email'];

				if(isset($_POST['auto'])){
					setcookie('auth', $user['secret'], time() + 364*24*3600, '/', null, false, true);
				}

				header('location: index.php?success=1');
				exit();

			}
			else {

				header('location: index.php?error=1&message=Impossible de vous authentifier correctement.');
				exit();
			}

		}

	}

?>

<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Temtelementator</title>
	<meta name="description" content="Max Lefort - Bienvenue sur le Temtelementator, trouvez facilement quel élément est fort ou faible face au votre !">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="design/default.css">
	<link href="https://fonts.googleapis.com/css?family=Bangers|Patrick+Hand&display=swap" rel="stylesheet">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon" >
</head>
<body>

	<?php include('src/header.php'); ?>

	<section>
		<div id="login-body">

			<?php if(isset($_SESSION['connect'])){  ?>

				<h1>Bonjour !</h1>
				<p>Le Temtelementator est maintenant à votre disposition !</p>

				<?php
				
				if(isset($_GET['success'])){

					echo'<div class="alert success">Vous êtes maintenant connecté.</div>';	
				} ?>
				<small><a id="deco" href="logout.php">Déconnexion</a></small>
		</div>

<!--------------------------------------------------------- LE TETELEMENTATOR --------------------------------------------------------->
			<div> 
					<h3>Selectionnez le type de votre TemTem :</h3>
					<br>
					<img class="fleche" src="images/fleche.png" alt="fleche">
					<br>
					<div class="select">
						<select id="mySelect" onchange="selectionner()">
						</select>
						<div class="select_arrow">
						</div>
					</div>
			</div>

					<p class="typos"><li id="type"></li></p>

			<div>
					<p><li id="fort"></li></p>
			</div>
					<hr>
			<div>
					<p><li id="faible"></li></p>
			</div>

		<div class="actu">
				<h2>Suivez l'actu TemTem</h2>
				<div id="accordion">
  					<h3>Rendez-vous sur TemtemFrance</h3>
  						<div>
						  <a target="_blank" rel="noopener" href="https://www.temtem-france.com/"><img class="logotemtemfr" src="images/logofulltemtemfr.png" alt=""></a>
  						</div>
  					<h3>Ou sur le Twitter TemtemFrance</h3>
  						<div>
							<a class="twitter-timeline" data-lang="fr" data-width="800" data-height="400" data-theme="dark" href="https://twitter.com/TemtemFrance?ref_src=twsrc%5Etfw">Tweets by TemtemFrance</a> 
							<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
						</div> 
				</div>
		</div>
<!--------------------------------------------------------- FIN TETELEMENTATOR --------------------------------------------------------->

			<?php } else {?>
					<h2>S'identifier</h2>

					<?php if(isset($_GET['error'])) {

						if(isset($_GET['message'])) {
							echo'<div class="alert error">'.htmlspecialchars($_GET['message']).'</div>';
						}

					} ?>

					<form method="post" action="index.php">
					
						<input type="email" name="email" placeholder="Votre adresse email" required />
						<input type="password" name="password" placeholder="Mot de passe" required />
					
						<button type="submit">S'identifier</button>
						<label id="option"><input type="checkbox" name="auto" checked />Se souvenir de moi</label>
					</form>
				

					<p class="grey">Première visite sur le Tetelementator ? <a href="inscription.php">Inscrivez-vous</a>.</p>
			
		</section>

					<div id="actu_twitter">
						<h2>Suivre l'actu Temtem</h2>			
						<a class="twitter-timeline" data-lang="fr" data-width="800" data-height="400" data-theme="dark" href="https://twitter.com/TemtemFrance?ref_src=twsrc%5Etfw">Tweets by TemtemFrance</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
					</div>	

					<div>
						<h2>Ou rendez-vous sur TemtemFrance</h2>
						<a target="_blank" rel="noopener" href="https://www.temtem-france.com/"><img src="images/logofulltemtemfr.png" alt=""></a>
					</div>		
			
			<?php } ?>


			<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
			<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>

<!----- Script de l'accordeon JQuery ----->
<script>
  		$( function() {
    	$( "#accordion" ).accordion({
			heightStyle: "content"
    	});
  		} );

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
</script>
<!-- Fin script accordeon -->


<!------------------------------------------------------- LE JS DU TEMTELEMENTATOR ------------------------------------------------------->
	<script>
		// Tableau des valeurs de la liste
		var tab = [
		{"nom":"Nature", "assoc": ["terreff", "eauff"],"faible":["feuf", "toxiquef"],"type":["nature"]}, 
		{"nom":"Eau", "assoc": ["feuff", "numeriqueff","terreff"],"faible":["naturef", "electricitef", "toxiquef"],"type":["eau"]},
		{"nom":"Feu", "assoc": ["natureff", "cristalff"],"faible":["terref", "eauf"],"type":["feu"]},
		{"nom":"Neutre", "assoc": ["rien"],"faible":["mentalf"],"type":["neutre"]},
		{"nom":"Numerique", "assoc": ["numeriqueff", "mentalff", "pugilatff"],"faible":["numeriquef", "electricitef", "eauf"],"type":["numerique"]},
		{"nom":"Toxique", "assoc": ["natureff", "eauff"],"faible":["ventf"],"type":["toxique"]},
		{"nom":"Electricite", "assoc": ["ventff", "mentalff" ,"numeriqueff", "eauff"],"faible":["terref", "cristalf"],"type":["electricite"]},
		{"nom":"Mental", "assoc": ["neutreff", "pugilatff"],"faible":["numeriquef", "electricitef", "cristalf"],"type":["mental"]},
		{"nom":"Vent", "assoc": ["terreff", "toxiqueff"],"faible":["electricitef"],"type":["vent"]}, 
		{"nom":"Terre", "assoc": ["electriciteff", "feuff", "cristalff"],"faible":["naturef", "pugilatf", "eauf", "ventf"],"type":["terre"]},
		{"nom":"Cristal", "assoc": ["electriciteff", "mentalff"],"faible":["feuf", "terref", "pugilatf"],"type":["cristal"]},
		{"nom":"Pugilat", "assoc": ["terreff", "cristalff"],"faible":["mentalf", "numeriquef"],"type":["pugilat"]}];

		// On remplit la liste dans le HTML à partir des éléments du tableau
		liste = document.getElementById('mySelect');
		tab.map(function(item) {
			var txtOption = `<option value="${item.nom}">${item.nom}</option>`;
		liste.innerHTML += txtOption;
		});

		// Fonction callback de sélection d'un élément de la liste
		function selectionner() {
		var item = tab.find(el => el.nom==document.getElementById('mySelect').value);

		// Afficher texte pour le "fort" et "sensible"
		fort.innerHTML = "<h3> Fort contre :</h3>";
		faible.innerHTML = "<h3> Sensible à :</h3>";
		type.innerHTML = "";

		// On vient chercher les images correspondantes au tableau par rapport à leurs noms
		item.assoc.map(function(item) {
			var txtLi = `<img src="images/fort/${item}.png" alt="${item}.png">`;
			fort.innerHTML += txtLi;
		});
		item.faible.map(function(item) {
			var txtLi = `<img src="images/sensible/${item}.png" alt="${item}.png"></<img>`;
			faible.innerHTML += txtLi;
		});
		item.type.map(function(item) {
			var txtLi = `<img src="images/${item}.png" alt="${item}.png"></<img>`;
			type.innerHTML += txtLi;
		});
		}

		// Premier appel pour initialiser la page
		selectionner();
	</script>
<!------------------------------------------------------- FIN JS DU TEMTELEMENTATOR ------------------------------------------------------->


	<?php include('src/footer.php'); ?>

</body>
</html>